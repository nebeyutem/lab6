#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#define MAX_PIZZAS 10
#define STUDENT_SLEEP_TIME 1

int pizzasConsumed = 0;
int slicesEaten = 10;
pthread_mutex_t pizzaAvailable;
pthread_mutex_t eatSlice;
pthread_mutex_t deliverPizza;
void *Student(void *args) {
	int *studentID = (int *)args;

	while (1)
	{
	// acquire slice to eat
	pthread_mutex_lock(&eatSlice);
	// acquire pizza to eat
	pthread_mutex_lock(&pizzaAvailable);

	if (slicesEaten == 10)
	{
		pizzasConsumed++;
		slicesEaten = 0;
		// Get a new pizza delivered
		pthread_mutex_unlock(&deliverPizza);

	if(pizzasConsumed != 11)

	printf("Student %d orders pizza %d\n", *studentID, pizzasConsumed);

	pthread_mutex_unlock(&eatSlice);

	sleep(STUDENT_SLEEP_TIME);

	continue;

	}

	if (pizzasConsumed == 11)

	{

	// Unlock mutexes and break loop

	pthread_mutex_unlock(&pizzaAvailable);

	pthread_mutex_unlock(&eatSlice);

	break;

	}

// Eat slice of pizza

slicesEaten++;

printf("Student %d eats Slice %d from pizza %d\n", *studentID, slicesEaten, pizzasConsumed);

// make pizza available

pthread_mutex_unlock(&pizzaAvailable);

// make slice available to be eaten

pthread_mutex_unlock(&eatSlice);

}

return NULL;

}

void *PizzaDelivery(void *args) {

while (1) {

// wait until pizza needs to be delivered

pthread_mutex_lock(&deliverPizza);

if(pizzasConsumed == 11) {

// Unlock mutex and break loop

pthread_mutex_unlock(&pizzaAvailable);

break;

}

printf("Pizza %d is delivered\n", pizzasConsumed);

// make pizza available

pthread_mutex_unlock(&pizzaAvailable);

}

return NULL;

}

int main(int argc, char *argv[]) {

int nStudents = atoi(argv[1]);

int i;

pthread_mutex_init(&pizzaAvailable, NULL);

pthread_mutex_init(&eatSlice, NULL);

pthread_mutex_init(&deliverPizza, NULL);

pthread_t thread_id;

int studentID[nStudents];

// no initial pizza delivery unless ordered

pthread_mutex_lock(&deliverPizza);

// create pizza delivery thread

pthread_create(&thread_id, NULL, PizzaDelivery, NULL);

for (i = 0; i < nStudents; i++)

{

studentID[i] = i + 1;

// create student thread with studentID

pthread_create(&thread_id, NULL, Student, (void *)&studentID[i]);

}

pthread_exit(NULL);

return 0;

}
